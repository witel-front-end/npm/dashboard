#!/usr/bin/env node

const { execSync } = require('child_process')

const runCommand = command => {
    try {
        execSync(`${command}`, { stdio: 'inherit' })
    } catch (e) {
        console.log(`Failed to execute ${command}`, e)
        return false
    }
    return true
}

const repoName = process.argv[2]

const gitCheckoutCommand = `git clone --depth 1 https://gitlab.com/witel-front-end/npm/expert-dashboard.git ${repoName}`
const installCheckoutCommand = `cd ${repoName} && npm install`

console.log(`Cloning the repository with name ${repoName}`)
const checkout = runCommand(gitCheckoutCommand)
if (!checkout) process.exit(-1)

console.log(`Installing dependencies for ${repoName}`)
const installedDeps = runCommand(installCheckoutCommand)
if (!installedDeps) process.exit(-1)

console.log(`Welcome to Witel dashboard!`)
console.log(`cd ${repoName} && enjoy!!`)